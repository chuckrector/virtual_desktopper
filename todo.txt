[X] Detect when virtual desktop changes
[X] Show virtual desktop # when switching desktops
[X] Keep window on top of all windows across all virtual desktops
[X] Remove window chrome and overlay virtual desktop # transparently
[X] Hide virtual desktop # after a timeout after each virtual desktop switch
